﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyDragger : MonoBehaviour
{
    private static DontDestroyDragger instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        instance = this;
    }
}
