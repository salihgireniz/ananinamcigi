﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public float multiplier = 1.5f;
    public float duration = 5f;
    private GameManager gm;
    public GameObject pickupEffect;
    public GameObject powerUpEffect;
    private GameObject Player;

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
    }
    
    void OnTriggerenter(Collider other)
    {

        if (other.tag == "Player")
        {
            //Instantiate(pickupEffect, transform.position, transform.rotation);
            Debug.Log("sada");
            StartCoroutine(Pickup());
        }
    }

    IEnumerator Pickup()
    {
        GameObject playerPower = Instantiate(powerUpEffect, Player.transform.position, Player.transform.rotation) as GameObject;
        //playerPower.transform.SetParent(Player.transform, false);
        gm.speedInc *= multiplier;
        gm.sliderCount *= multiplier;
        //GetComponent<MeshRenderer>().enabled = false;
        //GetComponent<Collider>().enabled = false;

        yield return new WaitForSeconds(duration);
        Destroy(playerPower);
        gm.speedInc /= multiplier;
        gm.sliderCount /= multiplier;
        Destroy(gameObject);
            

    }
}
