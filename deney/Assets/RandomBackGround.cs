﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomBackGround : MonoBehaviour
{
    public SpriteRenderer backgroundSpriteRenderer;
    public Sprite[] backgroundSprites;

    // Use this for initialization
    void Start()
    {
        backgroundSpriteRenderer.sprite = backgroundSprites[Random.Range(0, backgroundSprites.Length)];
    }
}
