﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackCounter : MonoBehaviour
{
    private float counTime;
    public Text countText;
    // Start is called before the first frame update
    void Start()
    {
        counTime = 4f; 
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (counTime > 0)
        {
            counTime -= 1 * Time.deltaTime;
            int countTimeToInt = (int)counTime;
            countText.text = ("SPIRITUAL!!!            " + countTimeToInt.ToString());
        }
        
    }
}
