﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColorer : MonoBehaviour
{
    Color randomColor;
    public List<Color> colorList= new List<Color>();
    void Start()
    {
        /*randomColor = new Color
        (
        Random.Range(0f, 1f),
        Random.Range(0f, 1f),
        Random.Range(0f, 1f),
        Random.Range(0f, 1f)
        );*/
        randomColor = colorList[Random.Range(0, colorList.Count)];
        GetComponent<MeshRenderer>().sharedMaterial.color = randomColor;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
