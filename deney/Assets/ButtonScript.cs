﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    private GameManager gm;
    private Slider slider;
    private GameObject Player;
    public GameObject Tail;

    void Start()
    {
        Tail = GameObject.FindGameObjectWithTag("Tail");
        Player = GameObject.FindGameObjectWithTag("Player");
        slider = GameObject.FindGameObjectWithTag("Slider").GetComponent<Slider>();
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
    }
    public void Restarter()
    {
        gm.highScoreText.enabled = false;
        gm.bestScore.enabled = false;
        gm.speedInc = 1f;
        slider.value = 0f;
        gm.countInc = 1f;
        gm.sliderCount = 1f;
        gm.GetComponent<AudioSource>().volume = 0.1f;
        Destroy(gameObject);
    }
}
