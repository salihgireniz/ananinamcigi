﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectStarter : MonoBehaviour
{
    GameObject tail;
    void Start()
    {
        tail = GameObject.FindGameObjectWithTag("Tail");
        tail.SetActive(false);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            tail.SetActive(true);
        }
    }
}
