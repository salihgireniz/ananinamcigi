﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixRotation : MonoBehaviour
{
    Quaternion rotation;
    void Awake()
    {
        rotation = transform.rotation;
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "EndPoint")
        {
            Debug.Log("tail gone");
            gameObject.SetActive(false);
        }
        
    }
    void FixedUpdate()
    {
        if (FindObjectOfType<PlayerCollision>().isDead == true)
        {
            gameObject.SetActive(false);
        }
    }
    void LateUpdate()
    {
        transform.rotation = rotation;
    }
}
