﻿using UnityEngine;

public class move : MonoBehaviour
{
    private static move instance;
    private GameManager gm;
    private Rigidbody rb;
    private float speed;
    private float speedInc;
    public Vector3 playerPos = new Vector3(0f, 0f, 0f);
    

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        instance = this;
        rb = GetComponent<Rigidbody>();;

    }
    void FixedUpdate()
    {
        speed = gm.speed;
        speedInc = gm.speedInc;
        rb.velocity = new Vector3(0f,0f,1f) * -speed * speedInc;
    }
}
