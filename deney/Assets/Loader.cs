﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{
    private GameManager gm;
    public GameObject contButton;
    public GameObject LevelCompleteText;
    private GameObject Grounds;
    public GameObject Tail;
    void Start()
    {
        Tail = GameObject.FindGameObjectWithTag("Tail");
        LevelCompleteText = GameObject.FindGameObjectWithTag("LevelCompleteText");
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        Grounds = GameObject.FindGameObjectWithTag("GroundManager");
    }
    void OnTriggerEnter(Collider other)
    {
        if( other.tag == "Tail")
        {
            Debug.Log("Loader OnTriggerEnter");
            gm.GetComponent<AudioSource>().volume = 0.01f;
            GetComponent<AudioSource>().Play();
            gm.speedInc = 0f;
            gm.countInc = 0f;
            other.GetComponent<MeshRenderer>().enabled = false;
            StartCoroutine(Waitt());
        }
        
    }
    IEnumerator Waitt()
    {
        LevelCompleteText.GetComponent<Text>().text = ("LEVEL COMPLETE!");
        yield return new WaitForSeconds(2);
        LevelCompleteText.GetComponent<Text>().text = ("NEXT LEVEL!");
        yield return new WaitForSeconds(1);
        LevelCompleteText.GetComponent<Text>().text = ("");
        if (gm.amnGroundOnScreen < 29 && gm.maxGround < 29)
        {
            gm.amnGroundOnScreen =gm.amnGroundOnScreen + 1;
        }
        else
        {
            gm.amnGroundOnScreen = gm.maxGround;
        }
        if(gm.level%2 == 0)
        {
            if (gm.necesserySpeed < 250f && gm.maxSpeed < 250f)
            {
                gm.necesserySpeed = gm.necesserySpeed + 10f;
            }
            gm.speed = gm.necesserySpeed;
        }
        
        else
        {
            gm.speed = gm.maxSpeed;
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Grounds.transform.position = Grounds.GetComponent<move>().playerPos;
        if(gm.level < 200)
        {
            gm.level += 1;
            
        }
        else
        {
            Debug.Log("You WIN");
        }
        GameObject Canvas = GameObject.FindGameObjectWithTag("Canvas") as GameObject;
        GameObject ContinueButton = Instantiate(contButton, new Vector3(0, 0, 1), Quaternion.identity);
        ContinueButton.transform.SetParent(Canvas.transform, false);
        Debug.Log("Instantiate button");

    }
}
