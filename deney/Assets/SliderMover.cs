﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SliderMover : MonoBehaviour
{
    private Slider slider;
    private float sliderCount;
    private GameManager gm;
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        slider = GameObject.FindGameObjectWithTag("Slider").GetComponent<Slider>();
        slider.value = 0f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        sliderCount = gm.sliderCount;
        slider.value += sliderCount * (100/((gm.amnGroundOnScreen+1)*100/gm.speed)) * Time.deltaTime;
    }
}
