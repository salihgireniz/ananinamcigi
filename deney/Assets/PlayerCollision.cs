﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using EZCameraShake;

public class PlayerCollision : MonoBehaviour
{
    private Rigidbody rb;


    private GameManager gm;
    public GameObject ReplayButton;
    private GameObject Player;
    public GameObject effect;
    private Slider slider;
    private GameObject Grounds;

    public float multiplier = 1.5f;
    public float duration = 5f;
    public GameObject powerUpText;
    private GameObject powerUpEffect;
    public bool isDead;
    void Start()
    {
        isDead = false;
        rb = GetComponent<Rigidbody>();
        powerUpEffect = GameObject.FindGameObjectWithTag("Effect");
        powerUpEffect.SetActive(false);
        slider = GameObject.FindGameObjectWithTag("Slider").GetComponent<Slider>();
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        Player = GameObject.FindGameObjectWithTag("Player");
        Grounds = GameObject.FindGameObjectWithTag("GroundManager");
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Obstacle")
        {
            Debug.Log("Is Dead");
            isDead = true;
            gm.highScoreText.enabled = true;
            gm.bestScore.enabled = true;
            Instantiate(effect, transform.position+new Vector3(0,2f,-2f), Quaternion.identity);
            gameObject.GetComponent<Renderer>().enabled= false;
            gameObject.GetComponent<Collider>().enabled = false;
            gm.GetComponent<AudioSource>().volume = 0.01f;
            GetComponent<AudioSource>().Play();
            CameraShaker.Instance.ShakeOnce(4f, 4f, .1f, 1f);
            gm.speedInc = 0f;
            gm.countInc = 0f;
            gm.sliderCount = 0f;
            StartCoroutine(Wait());
        }
        if (other.tag == "Gem")
        {
            other.GetComponent<AudioSource>().Play();
            StartCoroutine(CountBack());
            StartCoroutine(Pickup(other));
        }

    }
    
    IEnumerator Wait()
    {
        if (gm.speed > 200)
        {
            gm.speed -= 5f;
        }
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        gm.count = 0f;
        slider.value = 0f;
        Grounds.transform.position = Grounds.GetComponent<move>().playerPos;
        GameObject Canvas = GameObject.FindGameObjectWithTag("Canvas") as GameObject;
        GameObject replayButton = Instantiate(ReplayButton, new Vector3(0, 60, 1), Quaternion.identity) as GameObject;
        replayButton.transform.SetParent(Canvas.transform, false);
    }
    IEnumerator Pickup(Collider Gem)
    {
        
        Destroy(Gem);
        GetComponent<Collider>().enabled = false;
        powerUpEffect.SetActive(true);
        gm.speedInc *= multiplier;
        gm.sliderCount *= multiplier;

        GetComponent<MeshRenderer>().enabled = false;

        yield return new WaitForSeconds(duration);
        GetComponent<Collider>().enabled = true;
        GetComponent<MeshRenderer>().enabled = true;
        powerUpEffect.SetActive(false);
        gm.speedInc /= multiplier;
        gm.sliderCount /= multiplier;
    }

    IEnumerator CountBack()
    {
        GameObject Canvas = GameObject.FindGameObjectWithTag("Canvas") as GameObject;
        GameObject powerUpTextt = Instantiate(powerUpText, new Vector3(0, 300, 0), Quaternion.identity) as GameObject;
        powerUpTextt.transform.SetParent(Canvas.transform, false);
        yield return new WaitForSeconds(3);
        Destroy(powerUpTextt);

    }
    void FixedUpdate()
    {
        //rb.AddTorque(transform.right );
        //rb.AddForce(transform.forward * 1000f);
    }
}
