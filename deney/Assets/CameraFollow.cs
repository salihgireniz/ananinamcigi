﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private GameObject player;
    public Vector3 camPos;
    private float distanceZ;
    private float distanceY;
    public Material[] Skyboxes;

    
    void Awake()
    {
        RenderSettings.skybox = Skyboxes[Random.Range(0, Skyboxes.Length)];
        distanceZ = 45f;
        distanceY = 7f;
        player = GameObject.FindGameObjectWithTag("Player");
    }
    void FixedUpdate()
    {
        camPos = new Vector3(0, player.transform.position.y+distanceY, player.transform.position.z - distanceZ);
        gameObject.transform.position = camPos;
        gameObject.transform.rotation = Quaternion.identity;
    }
}
